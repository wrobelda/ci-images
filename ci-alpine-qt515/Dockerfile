FROM alpine

# Update container, import GPG key for KUQ
RUN apk update
# Install various basic packages
RUN apk add \
	gcc \
	g++ \
	make \
	samurai \
	cmake \
	flex \
	bison \
	py3-lxml \
	py3-paramiko \
	py3-yaml \
	py3-simplejson \
	wget \
	file \
	tar \
	gzip \
	go \
	rsync

# Install all Qt packages
RUN apk add \
	qt5-qt3d-dev \
	qt5-qtbase-dev \
	qt5-qtcharts-dev \
	qt5-qtconnectivity-dev \
	qt5-qtdatavis3d-dev \
	qt5-qtdeclarative-dev \
	qt5-qtdeclarative-dev \
	qt5-qtgamepad-dev \
	qt5-qtlocation-dev \
	qt5-qtlottie-dev \
	qt5-qtmultimedia-dev \
	qt5-qtnetworkauth-dev \
	qt5-qtpurchasing-dev \
	qt5-qtquick3d-dev \
	qt5-qtquickcontrols2-dev \
	qt5-qtremoteobjects-dev \
	qt5-qtscript-dev \
	qt5-qtscxml-dev \
	qt5-qtsensors-dev \
	qt5-qtserialbus-dev \
	qt5-qtspeech-dev \
	qt5-qtsvg-dev \
	qt5-qttools-dev \
	qt5-qtvirtualkeyboard-dev \
	qt5-qtwayland-dev \
	qt5-qtwebchannel-dev \
	qt5-qtwebengine-dev \
	qt5-qtwebglplugin-dev \
	qt5-qtwebsockets-dev \
	qt5-qtwebview-dev \
	qt5-qtx11extras-dev \
	qt5-qtxmlpatterns-dev
# And some other useful and base packages
RUN apk add git clang py3-sphinx py3-qt5 xvfb-run appstream py3-pip ruby-dev libffi-dev sassc dbus shadow
RUN pip install python-gitlab gcovr cppcheck_codequality

# KDE stuff also depends on the following
RUN apk add \
	# kdesrc-build
	perl-yaml-libyaml perl-json perl-io-socket-ssl \
	# kcodecs
	gperf \
	# modemmanager-qt
	modemmanager-dev \
	# networkmanager-qt
	networkmanager-dev \
	# kauth
	polkit-dev \
	# kwindowsystem
	xcb-util-keysyms-dev \
	xcb-util-wm-dev \
	# karchive
	bzip2-dev \
	xz-dev \
	# prison
	libdmtx-dev libqrencode-dev \
	# kimageformats
	openexr-dev libavif-dev \
	# kwayland
	wayland-dev \
	# okular
	exiv2-dev poppler-qt5-dev \
	# kfilemetadata
	attr-dev taglib-dev \
	# baloo
	lmdb-dev \
	# kdoctools
	perl-uri libxml2-dev libxslt-dev \
	# khtml
	giflib-dev openssl-dev \
	# kdnssd
	avahi-dev \
	# pim
	grantlee-dev \
	# khelpcenter (and pim for grantlee)
	xapian-core-dev \
	# sonnet
	hunspell-dev \
	# kio-extras and krdc, kio-fuse
	libssh-dev \
	# plasma-pa
	pulseaudio-dev libcanberra-dev \
	# sddm-kcm
	libxcursor-dev \
	# plasma-workspace
	libxtst-dev \
	# breeze-plymouth
	plymouth-dev \
	# kde-gtk-config/breeze-gtk
	gtk+3.0-dev gtk+2.0-dev py3-cairo \
	# plasma-desktop/discover
	appstream-dev fwupd-dev \
	# plasma-desktop
	xf86-input-synaptics-dev xf86-input-evdev-dev libxkbfile-dev xdg-user-dirs \
	# libksane
	sane-dev \
	# pim
	libical-dev \
	# <misc>
	alsa-lib-dev libraw-dev fftw-dev \
	# krita
	eigen-dev quazip-dev \
	# kaccounts / telepathy
	libaccounts-qt-dev signond-dev \
	# kwin
	libepoxy-dev xwayland-dev \
	# kcalc
	mpfr-dev \
	# kdevelop
	gdb \
	# wacomtablet
	libwacom-dev xf86-input-wacom-dev \
	# rust-qt-binding-generator
	rust cargo \
	# kdevelop
	clang clang-dev llvm-dev subversion-dev python3-dev \
	# libkleo
	gpgme-dev \
	# akonadi
	mariadb qt5-qtbase-mysql \
	# libkdegames
	libsndfile-dev \
	# ktp-common-internals (also rest of KDE Telepathy)
	telepathy-qt-dev \
	# audiocd-kio
	cdparanoia-dev \
	# ark
	libarchive-dev libzip-dev \
	# ffmpegthumbs
	ffmpeg-dev \
	# k3b
	flac-dev libmad-dev lame-dev libogg-dev libvorbis-dev libsamplerate-dev \
	# kamera
	libgphoto2-dev \
	# kdenlive
	mlt-dev rttr-dev \
	# print-manager
	cups-dev \
	# krfb
	libvncserver-dev \
	# minuet
	fluidsynth-dev \
	# kajongg
	py3-twisted \
	# okular
	texlive-full \
	# ksmtp tests
	cyrus-sasl-dev \
	# kdb
	mariadb-dev postgresql-dev \
	# Calligra, Krita and probably other things elsewhere too
	boost-dev \
	# Cantor, libqalculate-dev will be available in next Alpine release
	py3-numpy py3-matplotlib octave \
	# KPat
	freecell-solver-dev black-hole-solver-dev \
	# RKWard
	R-dev \
	# Keysmith
	libsodium-dev \
	# Plasma Phone Components
	libphonenumber-dev \
	# xdg-desktop-portal-kde
	pipewire-dev \
	# KSysGuard
	libnl3-dev \
	# kitinerary, qrca
	zxing-cpp-dev \
	# Neochat
	qcoro-dev libquotient-dev cmark-dev \
	# KWave
	audiofile-dev \
	# Various tests
	openbox

# For D-Bus to be willing to start it needs a Machine ID
RUN dbus-uuidgen > /etc/machine-id
# Certain X11 based software is very particular about permissions and ownership around /tmp/.X11-unix/ so ensure this is right
RUN mkdir /tmp/.X11-unix/ && chown root:root /tmp/.X11-unix/ && chmod 1777 /tmp/.X11-unix/

# We need a user account to do things as, and SSHD needs keys
RUN useradd -d /home/jenkins/ -u 1000 --user-group --create-home jenkins
